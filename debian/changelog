libnginx-mod-http-uploadprogress (1:0.9.3-1+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Wed, 26 Feb 2025 14:45:29 +0000

libnginx-mod-http-uploadprogress (1:0.9.3-1) unstable; urgency=medium

  * New upstream version 0.9.3
  * d/p/0001-nginx-1.24.0-compatibility.patch remove
  * d/control: bump Standards-Version: 4.7.0, no changes
  * d/gbp.conf: add [pull] track-missing = True
  * d/copyright: bump my copyright year

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Wed, 03 Jul 2024 19:11:23 +0200

libnginx-mod-http-uploadprogress (1:0.9.2-5) unstable; urgency=medium

  * d/control: remove Build-Depends nginx-abi-1.24.0-1

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Sat, 07 Oct 2023 15:31:28 +0200

libnginx-mod-http-uploadprogress (1:0.9.2-4) unstable; urgency=medium

  * d/p/0001-nginx-1.24.0-compatibility.patch added
  * NEW ABI: rebuild with nginx-abi-1.24.0-1

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Tue, 27 Jun 2023 23:16:40 +0200

libnginx-mod-http-uploadprogress (1:0.9.2-3+apertis2) apertis; urgency=medium

  * Bump changelog to trigger license scan report

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Thu, 27 Jul 2023 14:15:53 +0530

libnginx-mod-http-uploadprogress (1:0.9.2-3+apertis1) apertis; urgency=medium

  * Add debian/apertis/copyright

 -- Vignesh Raman <vignesh.raman@collabora.com>  Thu, 13 Apr 2023 19:35:51 +0530

libnginx-mod-http-uploadprogress (1:0.9.2-3+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to target.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 12 Apr 2023 20:03:50 +0530

libnginx-mod-http-uploadprogress (1:0.9.2-3) unstable; urgency=medium

  * d/t/generic rework. The test now checks module after
    installation/reload/restart.
  * d/control: bump Standards-Version: 4.6.2, no changes
  * d/gbb.conf: switched to debian branch main (debian-branch = main)
  * d/copyright: bump my copyright year
  * d/copyright: reformat text to be compatible with 'cme update dpkg-copyright'
  * NEW ABI: rebuild with nginx-abi-1.22.1-7

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Mon, 13 Feb 2023 12:56:47 +0100

libnginx-mod-http-uploadprogress (1:0.9.2-2) unstable; urgency=medium

  * d/control: added Multi-Arch: foreign

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Fri, 09 Dec 2022 12:50:30 +0100

libnginx-mod-http-uploadprogress (1:0.9.2-1) experimental; urgency=medium

  * Initial release. (Closes: 1024211)

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Wed, 30 Nov 2022 14:46:58 +0100
